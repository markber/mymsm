# MyMSM

## Short Intro to Utimaco HSM - Crypto-Server

> This page will show some basics with the Utimaco Crypto-Server SIMULATOR
> TEST with Version 4.45.3  /// Date 14.11.2021

### **First Topic**

Docker contaier with the HSM-Simulator

- **Requirements** 
```
for download the Utimaco Crypto-Server, you need an valid [Utimaco](https://utimaco.com) account.
```
- **Environment:**
```
create a working directory

    mkdir workdir  #change the directory to your own env.
    cd workdir

create a Dockerfile in your $workdir with your prefered editor, like nano, vi etc. -> see example Dockerfile

    nano Dockerfile

copy the following directory from the download source to your $workdir

    cp -r /media/cdrom/Software/Linux/Simulator/sim5_linux /workdir
```
- **Deploy Docker container / HSM-Simulator**
```
    docker build -t simulator:4.45.3 .
    
check Docker Image

    docker image ls
```
- **Start Docker container / HSM-Simulator**
```
docker run -dt --rm --name cssim4453 simulator:4.45.3

Verify Docker Log

    docker logs cssim4453

Check Docker container IP, you need the IP for the CAT or CSADM connection

    docker inspect cssim4453 | grep -m 1 \"IPAddress\"
    RESULT like: "IPAddress": "172.17.0.2"

Stop Docker

    docker stop cssim4453
```
### **Second Topic**

- **first connection** 
- **first steps**
